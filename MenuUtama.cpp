#include <stdio.h>

void mainMenu(){
	int inputNum;
	puts("============================================\n");
	puts(" <> <> <>	   GoVoNa 	    <><><>	\n");
	puts(" <> <> <>	 Menu Utama 	    <><><>	\n");
	puts("============================================\n");
	// printf("Username: %s   ||   Sisa Saldo: %lld \n", username, saldo);
	printf("\n");
	puts("____________________________________________\n");
	puts("[1] Top Up\n"); // pergi ke menu top up saldo govona
	puts("[2] Pulsa\n"); // pergi ke menu isi pulsa
	puts("[3] Tiket\n"); // pergi ke menu pembelian tiket
	puts("[4] Voucher Game\n"); // pergi ke menu pembelian voucher atau top up game
	puts("[5] Settings\n"); // pergi ke menu pengaturan
	puts("				     [0]EXIT\n");
	puts("____________________________________________\n");
	do {
		printf("Input a number: ");
		scanf("%d", &inputNum);
		getchar();
		switch (inputNum){
			case 1:
				// topupSaldo();
				break;
			case 2:
				// isiPulsa();
				break;
			case 3:
				// beliTiket();
				break;
			case 4:
				// topupGame();
				break;
			case 5:
				mainSettings();
				break;
			case 0:
				// endProgram();
				break;
			default:
				printf("Input is not recognized!\n");
				break;
			}
		} while (inputNum < 0 || inputNum > 5);
	}
}

int main (){
	mainMenu();
	return 0;
}
	

